import axios from 'axios';

export const apiProductConfig = axios.create({
  baseURL: process.env.NEXT_PUBLIC_PRODUCT_URL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

const apiConfig = axios.create({
  baseURL: '/api/',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});
export default apiConfig;
