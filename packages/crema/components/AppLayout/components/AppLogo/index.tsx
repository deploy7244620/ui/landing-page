import React from 'react';
import { useSidebarContext } from 'crema/context/AppContextProvider/SidebarContextProvider';
import { StyledAppLogo } from './index.styled';
import Image from 'next/image';
import Link from 'next/link';

type AppLogoProps = {
  hasSidebarColor?: boolean;
};
const AppLogo: React.FC<AppLogoProps> = ({ hasSidebarColor }) => {
  const { sidebarColorSet } = useSidebarContext();
  return (
    <StyledAppLogo>
      <Link href="/">
        {hasSidebarColor && sidebarColorSet.mode === 'dark' ? (
          <Image
            src='/assets/images/logo-white-with-name.png'
            alt='crema-logo'
            width={110}
            height={36}
            style={{
              width: 110,
              minWidth: 110
            }}
          />
        ) : (
          <Image
            src='/assets/images/logo-with-name.png'
            alt='crema-logo'
            width={110}
            height={36}
            style={{
              width: 110,
              minWidth: 110
            }}
          />
        )}
      </Link>
    </StyledAppLogo>
  );
};

export default AppLogo;
