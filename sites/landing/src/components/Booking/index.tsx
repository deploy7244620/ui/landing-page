import { Button } from 'antd'
import { FaPhoneAlt } from "react-icons/fa";
import ModalBooking from './ModalBooking';
import { useState } from 'react';

const Booking = () => {
    const [openModal, setOpenModal] = useState(false)

    return (
        <div style={{ marginTop: 10 }}>
            <Button
                size="large" 
                type="primary"
                icon={<FaPhoneAlt size={14} />}
                onClick={() => setOpenModal(true)}
            >
                Đặt bàn
            </Button>

            <ModalBooking
                open={openModal}
                onClose={() => setOpenModal(false)}
            />
        </div>
    )
}

export default Booking