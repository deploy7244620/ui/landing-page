import { Modal, Form, Row, Col, InputNumber, Input } from 'antd'

type Props = {
    open: boolean
    onClose: () => void
}

const ModalBooking = ({ open, onClose }: Props) => {
    return (
        <Modal
            open={open}
            onCancel={onClose}
        >
            <Form>
                <Row gutter={2}>
                    <Col>
                        <Form.Item label="Số lượng người">
                            <InputNumber />
                        </Form.Item>
                    </Col>

                    <Col>
                        <Form.Item label="Họ tên">
                            <InputNumber />
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={2}>
                    <Col>
                        <Form.Item label="Số điện thoại">
                            <InputNumber />
                        </Form.Item>
                    </Col>

                    <Col>
                        <Form.Item label="Email">
                            <InputNumber />
                        </Form.Item>
                    </Col>
                </Row>

                <Row>
                    <Form.Item label="Ghi chú">
                        <InputNumber />
                    </Form.Item>
                </Row>
            </Form>
        </Modal>
    )
}

export default ModalBooking