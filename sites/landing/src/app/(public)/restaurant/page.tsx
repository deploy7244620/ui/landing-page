'use client';
import React from 'react';
import RestaurantListing from './components/RestaurantListing';
import AppPageMeta from 'crema/components/AppPageMeta';
import RestaurantHeader from './components/RestaurantHeader';


const Restaurant = () => {
  const [filterData, setFilterData] = React.useState<any>({});

  return (
    <>
      <AppPageMeta title='Restaurants Listing' />
      <RestaurantHeader
        filterData={filterData}
        setFilterData={setFilterData}
      />
      <RestaurantListing
        filterData={filterData}
        setFilterData={setFilterData}
      />
    </>
  );
};

export default Restaurant;
