import { IoIosHelpCircleOutline } from "react-icons/io";
import { StyledRestaurantOpening, StyledRestaurantOpeningStatus } from "./index.styled";

const RestaurantOpening = ({ status, openingHours }: any) => {
    const today = new Date();
    const dayIndex = today.getDay();
    const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const opening = openingHours?.[daysOfWeek[dayIndex]]

    return (
        <StyledRestaurantOpening>
            <StyledRestaurantOpeningStatus open={status === 'OPEN'}>
                {status === 'OPEN' ? 'Đang mở cửa' : 'Đã đóng cửa'}
            </StyledRestaurantOpeningStatus>

            <div>
                <span>{opening?.start}</span>
                {opening?.start && <span>-</span>}
                <span>{opening?.end}</span>
            </div>
            <IoIosHelpCircleOutline />
        </StyledRestaurantOpening>
    )
}

export default RestaurantOpening