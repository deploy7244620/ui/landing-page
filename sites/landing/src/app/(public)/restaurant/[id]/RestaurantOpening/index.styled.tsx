import styled from 'styled-components';

export const StyledRestaurantOpening = styled.div`
    display: flex;
    gap: 5px;
    align-items: center;
    jsutify-content: center;
`;

export const StyledRestaurantOpeningStatus = styled.div<{ open: boolean }>`
    display: flex;
    gap: 5px;
    align-items: center;
    jsutify-content: center;
    color: ${({theme, open}) => open ? 'green' : 'red'};

    &::before {
        content: '';
        width: 10px;
        height: 10px;
        border-radius: 50%;
        background-color: ${({theme, open}) => open ? 'green' : 'red'};
        margin-right: 5px;
        margin-left: 2px;
    }
`;