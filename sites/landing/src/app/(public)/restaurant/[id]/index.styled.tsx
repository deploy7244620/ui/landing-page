import styled from 'styled-components';

export const StyledRestaurantDetail = styled.div`
  position: relative;
  padding-bottom: 15px;
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

export const StyledRestaurentBody = styled.div`
  display: flex;
  gap: 20px;
  width: 100%;
`;

export const StyledRestaurantMenu = styled.div`
  width: 300px;

  & .ant-menu {
    border-radius: ${({ theme }) => theme.cardRadius};
    border: none !important;
    padding: 10px;
  }
`;

export const StyledRestaurantBlock = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

export const StyledRestaurantBlockItem = styled.div`
  width: 100%;
  padding: 20px;
  border-radius: ${({ theme }) => theme.cardRadius};
  background: white;
`

export const StyledRestaurantBlockTitle = styled.div`
  font-size: ${({theme}) => theme.font.size.lg};
  font-weight:  ${({theme}) => theme.font.weight.bold};
  border-bottom: 1px solid ${({theme}) => theme.palette.borderColor};
  padding-bottom: 10px;
`

export const StyledRestaurantBlockBody = styled.div`
  padding-top: 15px;
`