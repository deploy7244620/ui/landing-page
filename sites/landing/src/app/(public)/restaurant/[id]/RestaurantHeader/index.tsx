import { StyledProductCuisine, StyledProductDescription, StyledProductName, StyledRestaurantHeader, StyledRestaurantInfo, StyledRestaurantInfoLine, StyledRestaurantListSlider, StyledRestaurantListSliderThumb, StyledRestaurantScoial, StyledRestaurantTitle } from "./index.styled";
import Slider from 'react-slick';
import Image from 'next/image';
import { FaFacebook } from "react-icons/fa6";
import Link from 'next/link';
import { RiInstagramFill } from "react-icons/ri";
import { RiGlobalFill } from "react-icons/ri";
import RestaurantOpening from '../RestaurantOpening';
import { FaMapMarkerAlt, FaPhoneAlt } from "react-icons/fa";
import Booking from "@/components/Booking";

const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
};

interface RestaurantHeaderProps {
    data: any
}

const RestaurantHeader = ({ data }: RestaurantHeaderProps) => {
    return (
        <StyledRestaurantHeader>
            <StyledRestaurantListSlider>
                <Slider className='slick-slider-global' {...settings}>
                    {data?.imagePaths.map((src: string) => {
                    return (
                        <StyledRestaurantListSliderThumb key={src}>
                            <Image
                                src={`${src}`}
                                alt='watch'
                                width={400}
                                height={100}
                            />
                        </StyledRestaurantListSliderThumb>
                    );
                    })}
                </Slider>
            </StyledRestaurantListSlider>

            <StyledRestaurantInfo>
                <StyledRestaurantTitle>
                    <StyledProductCuisine>{data?.cuisineType}</StyledProductCuisine>
                    <StyledProductName>{data?.name}</StyledProductName>
                    <StyledProductDescription>{data?.description}</StyledProductDescription>
                </StyledRestaurantTitle>

                <RestaurantOpening
                    status={data?.statusRestaurants}
                    openingHours={data?.openingHours}
                />

                <StyledRestaurantInfoLine>
                    <FaMapMarkerAlt /> <span>{data?.address}</span>
                </StyledRestaurantInfoLine>

                <StyledRestaurantInfoLine>
                    <FaPhoneAlt /> <span>{data?.Phone}</span>
                </StyledRestaurantInfoLine>

                <StyledRestaurantScoial>
                    {data?.socialMediaLinks?.facebook && (
                    <Link href={data?.socialMediaLinks?.facebook} target="_blank">
                        <FaFacebook size={30} />
                    </Link>
                    )}

                    {data?.socialMediaLinks?.instagram && (
                    <Link href={data?.socialMediaLinks?.instagram} target="_blank">
                        <RiInstagramFill size={30} />
                    </Link>
                    )}

                    {data?.socialMediaLinks?.other && (
                    <Link href={data?.socialMediaLinks?.other} target="_blank">
                        <RiGlobalFill size={30} />
                    </Link>
                    )}
                </StyledRestaurantScoial>

                <Booking />
            </StyledRestaurantInfo>
        </StyledRestaurantHeader>
    )
}

export default RestaurantHeader