import styled from 'styled-components';

export const StyledRestaurantHeader = styled.div`
  background-color: white;
  display: flex;
  gap: 20px;
  padding: 20px;
  border-radius: ${({ theme }) => theme.cardRadius};
`;

export const StyledRestaurantListSlider = styled.div`
  margin-bottom: 12px;
  max-width: 500px;
  width: 400px;

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
    padding-right: 16px;
    margin-bottom: 0;
    width: 400px;

    [dir='rtl'] & {
      padding-right: 0;
      padding-left: 16px;
    }
  }

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.lg}px) {
    width: 400px;
  }

  & .slick-dots {
    bottom: -15px;
  }
`;

export const StyledRestaurantListSliderThumb = styled.div`
  margin-bottom: 8px;
  text-align: center;

  & img {
    max-width: 100%;
    display: inline-block;
  }
`;

export const StyledRestaurantInfo = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;

export const StyledRestaurantTitle = styled.div``;

export const StyledProductCuisine = styled.p`
  color: ${({theme}) => theme.palette.text.secondary};
  font-size: ${({ theme }) => theme.font.size.base};
`;

export const StyledProductName = styled.h1`
  margin: 0;
  padding: 0;
  color: ${({theme}) => theme.palette.text.primary};
  font-size: ${({ theme }) => theme.font.size.xl};
`;

export const StyledProductDescription = styled.p`
  color: ${({theme}) => theme.palette.text.secondary};
  font-size: ${({ theme }) => theme.font.size.base};
`;

export const StyledRestaurantInfoLine = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`;

export const StyledRestaurantScoial = styled.div`
  display: flex;
  gap: 15px;
  margin-top: 15px;
`;