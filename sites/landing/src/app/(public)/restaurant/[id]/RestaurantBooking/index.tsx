import Booking from "@/components/Booking"
import { StyledRestaurantBlockBody, StyledRestaurantBlockItem, StyledRestaurantBlockTitle } from "../index.styled"

const RestaurantBooking = () => {
    return (
        <StyledRestaurantBlockItem id="booking">
            <StyledRestaurantBlockTitle>Đặt bàn</StyledRestaurantBlockTitle>
            <StyledRestaurantBlockBody>
                <Booking />
            </StyledRestaurantBlockBody>
        </StyledRestaurantBlockItem>
    )
}

export default RestaurantBooking