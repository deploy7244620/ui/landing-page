'use client';
import React, { useEffect, useRef, useState } from 'react';
import AppPageMeta from 'crema/components/AppPageMeta';
import { useParams } from 'next/navigation';
import { StyledRestaurantDetail, StyledRestaurantBlock, StyledRestaurantMenu, StyledRestaurentBody } from './index.styled';
import { useGetDataProductApi } from 'crema/hooks/APIHooks';
import { FaPhoneAlt } from "react-icons/fa";
import { Menu } from 'antd'
import { MdOutlineRestaurantMenu } from "react-icons/md";
import { IoIosAlbums } from "react-icons/io";
import { MdOutlineRateReview } from "react-icons/md";
import RestaurantMenu from './RestaurantMenu'
import RestaurantAlbum from './RestaurantAlbum'
import RestaurantMap from './RestaurantMap'
import RestaurantReview from './RestaurantReview'
import { FaMapLocationDot } from "react-icons/fa6";
import RestaurantBooking from './RestaurantBooking';
import RestaurantHeader from './RestaurantHeader';
import AppLoader from 'crema/components/AppLoader';

const items = [
  // {
  //   icon: <FaPhoneAlt />,
  //   label: 'Đặt bàn',
  //   key: 'booking',
  // },
  {
    icon: <MdOutlineRestaurantMenu />,
    label: 'Menu',
    key: 'menu',
  },
  {
    icon: <IoIosAlbums />,
    label: 'Albums',
    key: 'album',
  },
  {
    icon: <MdOutlineRateReview />,
    label: 'Đánh giá',
    key: 'review',
  },
  {
    icon: <FaMapLocationDot />,
    label: 'Map',
    key: 'map',
  },
];

const RestaurantDetail = () => {
  const params = useParams();
  const { id } = params;
  const [isFixed, setIsFixed] = useState(false);
  const refMenu = useRef(null)
  const [{ apiData, loading }, { setQueryParams }] =
    useGetDataProductApi<any>('restaurants', undefined, { id }, false);
  const currentRestaurant = apiData?.result?.data?.[0]

  useEffect(() => {
    const handleScroll = () => {
      const boxTop = (refMenu?.current as any)?.getBoundingClientRect()?.top;

      // Kiểm tra xem box đã scroll đến viewport chưa
      if (boxTop <= 70) {
        setIsFixed(true);
      } else {
        setIsFixed(false);
      }
    };

    // Lắng nghe sự kiện scroll
    window.addEventListener('scroll', handleScroll);

    // Dọn dẹp sự kiện khi component unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [refMenu]);

  useEffect(() => {
    setQueryParams({ id });
  }, [id]);

  const handleScrollToView = (item: any) => {
    (document.getElementById(item.key) as any).scrollIntoView({
      behavior: 'smooth',
      block: 'center',
      inline: 'nearest'
  });
  }

  if (loading) return (
    <>
      <AppPageMeta title='Product Details' />
      <AppLoader />
    </>
  )

  return (
    <StyledRestaurantDetail>
      <AppPageMeta title={currentRestaurant?.name} />
      
      <RestaurantHeader data={currentRestaurant} />

      <StyledRestaurentBody>
        <StyledRestaurantMenu ref={refMenu}>
          <div
            style={{
              width: '300px',
              position: isFixed ? 'fixed' : 'static',
              top: isFixed ? '80px' : 'auto',
              zIndex: isFixed ? '1000' : 'auto'
            }}
          >
            <Menu mode="vertical" items={items} selectable={false} onClick={handleScrollToView} />
          </div>
        </StyledRestaurantMenu>

        <StyledRestaurantBlock>
          {/* <RestaurantBooking /> */}
          <RestaurantMenu data={currentRestaurant?.facilities} />
          <RestaurantAlbum images={currentRestaurant?.imagePaths} />
          <RestaurantReview />
          <RestaurantMap />
        </StyledRestaurantBlock>
      </StyledRestaurentBody>
    </StyledRestaurantDetail>
  );
};

export default RestaurantDetail;
