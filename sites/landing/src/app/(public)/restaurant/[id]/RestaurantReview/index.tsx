import { StyledRestaurantBlockBody, StyledRestaurantBlockItem, StyledRestaurantBlockTitle } from "../index.styled"
import ReviewCell from "./ReviewCell";
import ReviewInfo from "./ReviewInfo"
import AppList from 'crema/components/AppList';
import { Divider } from 'antd';

const RestaurantReview = () => {
    return (
        <StyledRestaurantBlockItem id="review">
            <StyledRestaurantBlockTitle>Review</StyledRestaurantBlockTitle>
            <StyledRestaurantBlockBody>
                <ReviewInfo />
                <Divider style={{ marginTop: 15, marginBottom: 15 }} />
                <AppList
                    data={[1, 2, 3, 4, 5]}
                    renderItem={(data) => <ReviewCell key={data} />}
                />
            </StyledRestaurantBlockBody>
        </StyledRestaurantBlockItem>
    )
}

export default RestaurantReview