import { StyledRestaurantBlockBody, StyledRestaurantBlockItem, StyledRestaurantBlockTitle } from "../index.styled"
import { SlytedRestaurantMenu, SlytedRestaurantMenuItem, SlytedRestaurantMenuItemName, SlytedRestaurantMenuItemThumb } from "./index.styled"
import { MdFastfood } from "react-icons/md";

const RestaurantMenu = ({ data }: any) => {
    return (
        <StyledRestaurantBlockItem id="menu">
            <StyledRestaurantBlockTitle>Menu</StyledRestaurantBlockTitle>
            <StyledRestaurantBlockBody>
                <SlytedRestaurantMenu>
                    {data?.map((item: any, index: number) => (
                        <SlytedRestaurantMenuItem key={index} status={item?.status}>
                            <SlytedRestaurantMenuItemThumb>
                                <MdFastfood />
                            </SlytedRestaurantMenuItemThumb>

                            <SlytedRestaurantMenuItemName>
                                <h3>{item?.name}</h3>
                                <p>{item?.description}</p>
                            </SlytedRestaurantMenuItemName>
                        </SlytedRestaurantMenuItem>
                    ))}
                </SlytedRestaurantMenu>
            </StyledRestaurantBlockBody>
        </StyledRestaurantBlockItem>
    )
}

export default RestaurantMenu