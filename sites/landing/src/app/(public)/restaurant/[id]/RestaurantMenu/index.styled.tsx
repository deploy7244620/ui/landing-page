import styled from 'styled-components';

export const SlytedRestaurantMenu = styled.div<{ status?: boolean; }>`
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    width: 100%;
`;

export const SlytedRestaurantMenuItem = styled.div<{ status?: boolean; }>`
    opacity: ${({status}) => status ? 1 : 0.25};
    width: calc(50% - 20px);
    border-bottom: 1px solid #f0f0f0;
    padding-bottom: 10px;
    display: flex;
    gap: 10px;
`;

export const SlytedRestaurantMenuItemName = styled.div`
    & h3 {
        font-size: ${({theme}) => theme.font.size.base};
        font-weight: ${({theme}) => theme.font.weight.bold};
    }

    & p {
        color: ${({theme}) => theme.palette.text.secondary};
    }
`;

export const SlytedRestaurantMenuItemThumb = styled.div`
    width: 40px;
    min-width: 40px;
    max-width: 40px;
    height: 40px;
    min-height: 40px;
    max-height: 40px;
    border-radius: 4px;
    background-color: ${({theme}) => theme.palette.background.default};
    display: flex;
    align-items: center;
    justify-content: center;
`;