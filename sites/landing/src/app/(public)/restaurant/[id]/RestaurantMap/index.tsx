'use client';
import React from 'react';
import { StyledRestaurantBlockBody, StyledRestaurantBlockItem, StyledRestaurantBlockTitle } from '../index.styled';
import { GoogleMap } from '@react-google-maps/api';

const RestaurantMap = () => {
  return (
    <StyledRestaurantBlockItem id="map">
      <StyledRestaurantBlockTitle>Map</StyledRestaurantBlockTitle>
      <StyledRestaurantBlockBody style={{ height: 500 }}>
        <GoogleMap
          zoom={15}
          mapContainerStyle={{ width: '100%', height: '100%' }}
          options={{
            scrollwheel: false,
          }}
          center={{ lat: 47.646935, lng: -122.303763 }}
        />
      </StyledRestaurantBlockBody>
    </StyledRestaurantBlockItem>
  )
};

export default RestaurantMap;
