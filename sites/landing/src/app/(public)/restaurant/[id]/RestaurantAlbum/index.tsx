import { StyledRestaurantBlockBody, StyledRestaurantBlockItem, StyledRestaurantBlockTitle } from "../index.styled"
import { Row, Col, Image } from 'antd';

interface RestaurantAlbumProps {
    images: any
}

const RestaurantAlbum = ({ images }: RestaurantAlbumProps) => {
    return (
        <StyledRestaurantBlockItem id="album">
            <StyledRestaurantBlockTitle>Albums</StyledRestaurantBlockTitle>
            <StyledRestaurantBlockBody>
                <Row gutter={[16, 16]}>
                    {images?.map((src: string, index: number) => (
                        <Col xs={24} sm={12} md={8} lg={6} key={index}>
                            <Image src={src} />
                        </Col>
                    ))}
                </Row>
            </StyledRestaurantBlockBody>
        </StyledRestaurantBlockItem>
    )
}

export default RestaurantAlbum