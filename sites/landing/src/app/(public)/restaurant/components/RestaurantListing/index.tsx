import React, { useEffect, useState } from 'react';
import {
  StyledRestaurantListMainContent,
  StyledRestaurantListView,
} from './index.styled';
import { useGetDataProductApi } from 'crema/hooks/APIHooks';
import RestaurantGrid from './RestaurantGrid';
import type {
  ProductDataFilterType,
} from 'crema/types/models/ecommerce/EcommerceApp';

type Props = {
  filterData: ProductDataFilterType;
  setFilterData: (filterData: ProductDataFilterType) => void;
};

const RestaurantListing = ({
  filterData,
  setFilterData,
}: Props) => {
  const [page, setPage] = useState(0);
  const [{ apiData, loading }, { setQueryParams }] =
    useGetDataProductApi<any>(
      'restaurants',
      {},
      {},
      false,
    );
    
  const searchProduct = (title: string) => {
    setFilterData({ ...filterData, title });
  };

  const onPageChange = (value: number) => {
    setPage(value);
  };

  useEffect(() => {
    setQueryParams({
      page,
      ...filterData,
    });
  }, [filterData]);

  return (
    <StyledRestaurantListView>
      <StyledRestaurantListMainContent>
        <RestaurantGrid ecommerceList={apiData?.result?.data} loading={loading} />
      </StyledRestaurantListMainContent>
    </StyledRestaurantListView>
  );
};

export default RestaurantListing;
