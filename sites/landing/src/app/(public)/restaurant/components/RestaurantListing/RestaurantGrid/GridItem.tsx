import React, { useState } from 'react';
import IntlMessages from 'crema/helpers/IntlMessages';
import { useRouter } from 'next/navigation';
import { HeartFilled, HeartOutlined, StarOutlined } from '@ant-design/icons';
import {
  StyledProductGridInfo,
  StyledProductGridInfoLine,
  StyledProductGridCard,
  StyledProductGridCardHeader,
  StyledProductGridCardHeaderBadge,
  StyledProductGridCardHeaderThumb,
  StyledProductGridCardPara,
  StyledProductGridCardTitle,
  StyledProductListFavorCheck,
} from './index.styled';
import type { ProductDataType } from 'crema/types/models/ecommerce/EcommerceApp';
import Image from 'next/image';
import { FaMapMarkerAlt, FaPhoneAlt } from "react-icons/fa";
import Link from 'next/link';

type Props = {
  item: any;
};
const GridItem = (props: Props) => {
  const { item } = props;
  const [isFavorite, setIsFavorite] = useState(false);
  const router = useRouter();

  const OnFavorite = () => {
    setIsFavorite(!isFavorite);
  };

  return (
    <StyledProductGridCard
      className='item-hover'
    >
      <StyledProductGridCardHeader>
        <StyledProductGridCardHeaderBadge>
          {item?.capacity ?? 0}
          <StarOutlined />
        </StyledProductGridCardHeaderBadge>

        <StyledProductGridCardHeaderThumb>
          <Image
            src={`${item?.imagePaths?.[0]}`}
            alt='watch'
            height={219}
            width={191}
            // sizes='100vw'
            style={{
              width: 'auto',
            }}
          />
        </StyledProductGridCardHeaderThumb>

        <StyledProductListFavorCheck onClick={OnFavorite}>
          {isFavorite ? <HeartFilled /> : <HeartOutlined />}
        </StyledProductListFavorCheck>
      </StyledProductGridCardHeader>

      <Link href={`/restaurant/${item?._id}`}>
        <StyledProductGridCardTitle className='text-truncate'>
          {item?.name}
        </StyledProductGridCardTitle>
      </Link>

      <StyledProductGridCardPara className='text-truncate'>
        {item?.description}
      </StyledProductGridCardPara>

      <StyledProductGridInfo>
        <StyledProductGridInfoLine>
          <FaMapMarkerAlt /> <span>{item?.address}</span>
        </StyledProductGridInfoLine>

        <StyledProductGridInfoLine>
          <FaPhoneAlt /> <span>{item?.Phone}</span>
        </StyledProductGridInfoLine>
      </StyledProductGridInfo>
    </StyledProductGridCard>
  );
};

export default GridItem;
