import React from "react";
import AppGrid from "crema/components/AppGrid";
import GridItem from "./GridItem";
import ListEmptyResult from "crema/components/AppList/ListEmptyResult";
import type { ProductDataType } from "crema/types/models/ecommerce/EcommerceApp";

type Props = {
  ecommerceList: ProductDataType[];
  loading: boolean;
};
const RestaurantGrid = ({ ecommerceList, loading }: Props) => (
  <AppGrid
    itemPadding={8}
    delay={200}
    responsive={{
      xs: 1,
      sm: 3,
      xxl: 4,
    }}
    data={ecommerceList}
    renderItem={(item) => <GridItem item={item} key={item._id} />}
    ListEmptyComponent={
      <ListEmptyResult content="No product found" loading={loading} />
    }
  />
);
export default RestaurantGrid;
