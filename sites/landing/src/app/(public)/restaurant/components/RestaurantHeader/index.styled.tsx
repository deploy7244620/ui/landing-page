import { Button, Input } from 'antd';
import { rgba } from 'polished';
import styled from 'styled-components';

const { Search } = Input;

export const StyledRestaurantHeader = styled.div`
  width: 100%;
  background-color: ${({ theme }) => theme.palette.background.paper};
  border-radius: ${({theme}) => theme.cardRadius};
  margin-bottom: 20px;
`;

export const StyledRestaurantHeaderFixed = styled.div<{ fixed?: boolean }>`
  width: 100%;
  background-color: ${({ theme }) => theme.palette.background.paper};
  padding: 20px 0;
  border-bottom: ${({fixed}) => fixed ? '1px' : '0'} solid ${({ theme }) => theme.palette.borderColor};
  border-radius: ${({theme, fixed}) => fixed ? 0 : theme.cardRadius};
`;

export const StyledRestaurantHeaderFixedContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  gap: 15px;
  margin: 0 auto;
  margin-left: auto;
  margin-right: auto;
  padding-left: 20px;
  padding-right: 20px;

  @media screen and (min-width: ${({theme}) => theme.breakpoints.xl}px) {
    max-width: 1140px;
  }

  @media screen and (min-width: ${({theme}) => theme.breakpoints.xxl}px) {
    max-width: 1440px;
  }

  @media screen and (min-width: 1920px) {
    max-width: 1720px;
  }
`;