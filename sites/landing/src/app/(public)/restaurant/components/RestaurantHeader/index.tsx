import React, { useEffect, useRef, useState } from 'react';
import { FaMapMarkerAlt } from "react-icons/fa";
import {
  StyledRestaurantHeader,
  StyledRestaurantHeaderFixed,
  StyledRestaurantHeaderFixedContainer,
} from './index.styled';
import provinces from 'crema/constants/province'
import { Select, Input } from "antd";

const { Option } = Select;

type Props = {
  filterData: any;
  setFilterData: React.Dispatch<React.SetStateAction<any>>;
};

const ProductHeader = ({ filterData, setFilterData }: Props) => {
  const [isFixed, setIsFixed] = useState(false);
  const refHeader = useRef(null)

  useEffect(() => {
    const handleScroll = () => {
      const boxTop = (refHeader?.current as any)?.getBoundingClientRect()?.top;

      // Kiểm tra xem box đã scroll đến viewport chưa
      if (boxTop <= 70) {
        setIsFixed(true);
      } else {
        setIsFixed(false);
      }
    };

    // Lắng nghe sự kiện scroll
    window.addEventListener('scroll', handleScroll);

    // Dọn dẹp sự kiện khi component unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [refHeader]);

  return (
    <StyledRestaurantHeader ref={refHeader}>
      <StyledRestaurantHeaderFixed
        fixed={isFixed}
        style={{
          position: isFixed ? 'fixed' : 'static',
          top: isFixed ? '72px' : 'auto',
          left: isFixed ? '0px' : 'auto',
          zIndex: isFixed ? '1000' : 'auto'
        }}
      >
        <StyledRestaurantHeaderFixedContainer>
          <Select
            size="large"
            placeholder="Tỉnh / Thành phố"
            onChange={(value) => {
              setFilterData((prev: any) => ({
                ...prev,
                inStock: [value === 1],
              }));
            }}
            style={{ width: '300px' }}
            showSearch
          >
            {provinces.map((item: any) => (
              <Option key={item.Id} value={item.Name}>
                {item.Name}
              </Option>
            ))}
          </Select>

          <Input.Search
            size="large"
            onChange={(value) => {
              setFilterData((prev: any) => ({
                ...prev,
                keyword: value,
              }));
            }}
          />
        </StyledRestaurantHeaderFixedContainer>
      </StyledRestaurantHeaderFixed>
    </StyledRestaurantHeader>
  );
};

export default ProductHeader;
