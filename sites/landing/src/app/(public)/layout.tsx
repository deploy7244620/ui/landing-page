'use client';
import React from 'react';
import routesConfig from '../routesConfig';
import { Layouts } from 'crema/components/AppLayout';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { NavStyle } from 'crema/constants/AppEnums';

export default function RootLayout({ children }: any) {
  const AppLayout = Layouts[NavStyle.HOR_HEADER_FIXED];

  return <AppLayout routesConfig={routesConfig}>{children}</AppLayout>;
}
