const routesConfig = [
    {
      id: 'restaurant',
      title: 'Nhà hàng',
      messageId: 'nav.restaurant',
      path: '/restaurant',
    },
    {
      id: 'booking',
      title: 'Đặt bàn',
      messageId: 'nav.booking',
      path: '/dat-ban',
    },
  ];
  
  export default routesConfig;