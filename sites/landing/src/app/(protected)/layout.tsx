'use client';
import React, { useEffect } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';
import { useAuthUser } from 'crema/hooks/AuthHooks';
import AppLoader from 'crema/components/AppLoader';
import routesConfig from '../routesConfig';
import { Layouts } from 'crema/components/AppLayout';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { NavStyle } from 'crema/constants/AppEnums';

export default function RootLayout({ children }: any) {
  const AppLayout = Layouts[NavStyle.HOR_HEADER_FIXED];

  const searchParams = useSearchParams();

  const { user, isLoading } = useAuthUser();
  const router = useRouter();
  const queryParams = searchParams.toString();

  useEffect(() => {
    if (!user && !isLoading) {
      router.push('/signin' + (queryParams ? '?' + queryParams : ''));
    }
  }, [user, isLoading, queryParams]);

  if (!user || isLoading) return <AppLoader />;

  return <AppLayout routesConfig={routesConfig}>{children}</AppLayout>;
}
