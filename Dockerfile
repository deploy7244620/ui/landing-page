FROM node:20.13.1
WORKDIR /app
COPY package.json ./
COPY . .
RUN yarn 
RUN yarn build
ENV TZ=Asia/Ho_Chi_Minh
ENV NODE_ENV=production
CMD yarn start
EXPOSE 3000

